// https://inclusive-components.design/notifications/

const Toaster = function ({liveRegion = null, defaultDuration = 5000, entranceAnimation = "", exitAnimation = ""}) {
	if (null === liveRegion) return console.warn("Toaster: No live region!");

	// Remove toast
	const removeToast = function (toast) {
		// No animation class specified
		if (!exitAnimation) {
			if (liveRegion.contains(toast)) liveRegion.removeChild(toast);
			return;
		}

		// Animate then remove node
		toast.addEventListener("animationend", function (e) {
			if (liveRegion.contains(toast)) liveRegion.removeChild(toast);
		});
		toast.classList.add(exitAnimation);
	};

	// Notify
	const notify = function (type, title, message, duration = defaultDuration) {
		let note = document.createElement("p");
		type = type.toLowerCase();

		note.classList.add("toast", `toast--${type}`);
		if (entranceAnimation) note.classList.add(entranceAnimation);

		note.innerHTML = `
						<svg class="icon" viewBox="0 0 20 20" width="1em" focusable="false">
							<use xlink:href="#${type}"></use>
						</svg>
						<span class="title">${title}</span>
						<span class="message">${message}</span>
					`;

		liveRegion.appendChild(note);

		window.setTimeout(() => {
			removeToast(note);
		}, duration);
	};

	// Dismissable toasts
	liveRegion.addEventListener("click", function (e) {
		if (e.target && e.target.closest(".toast")) {
			e.stopPropagation();
			e.stopImmediatePropagation();
			e.preventDefault();
			removeToast(e.target.closest(".toast"));
		}
	});

	return {
		notify: notify,
	};
};
